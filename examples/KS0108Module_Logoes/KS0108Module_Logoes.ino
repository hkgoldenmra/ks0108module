#include <KS0108Module.h>

KS0108Module ks0108Module = KS0108Module(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, A0, A1, A2);

void setup() {
	Font unifontAscii = Font(Font::UNIFONT_ASCII());
	ks0108Module.initial();
	ks0108Module.clearAll();
	ks0108Module.selectScreen(KS0108Module::SCREEN_LEFT);
	ks0108Module.setScreen(true);
	ks0108Module.setStartLine(0);
	ks0108Module.drawText(0, 0, "hkgolden", unifontAscii);
	ks0108Module.drawText(2, 0, " youtube", unifontAscii);
	ks0108Module.drawText(4, 0, " Maya, 3", unifontAscii);
	ks0108Module.drawText(6, 0, "      He", unifontAscii);
	ks0108Module.selectScreen(KS0108Module::SCREEN_CENTER);
	ks0108Module.setScreen(true);
	ks0108Module.setStartLine(0);
	ks0108Module.drawText(0, 0, "mra.blog", unifontAscii);
	ks0108Module.drawText(2, 0, ".hk/hkgo", unifontAscii);
	ks0108Module.drawText(4, 0, "Ds Max, ", unifontAscii);
	ks0108Module.drawText(6, 0, "llo, Wor", unifontAscii);
	ks0108Module.selectScreen(KS0108Module::SCREEN_RIGHT);
	ks0108Module.setScreen(true);
	ks0108Module.setStartLine(0);
	ks0108Module.drawText(0, 0, "spot.com", unifontAscii);
	ks0108Module.drawText(2, 0, "ldenmra ", unifontAscii);
	ks0108Module.drawText(4, 0, "Blender ", unifontAscii);
	ks0108Module.drawText(6, 0, "ld      ", unifontAscii);
}

void loop() {
}