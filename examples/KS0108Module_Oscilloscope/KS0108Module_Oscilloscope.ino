#include <KS0108Module.h>

const byte RS = 2;
const byte RW = 3;
const byte EN = 4;
const byte D0 = 5;
const byte D1 = 6;
const byte D2 = 7;
const byte D3 = 8;
const byte D4 = 9;
const byte D5 = 10;
const byte D6 = 11;
const byte D7 = 12;
const byte CA = A0;
const byte CB = A1;
const byte RE = A2;
const byte PROBE = A5;

KS0108Module ks0108Module = KS0108Module(RS, RW, EN, D0, D1, D2, D3, D4, D5, D6, D7, CA, CB, RE);
const byte SCREENS[] = {KS0108Module::SCREEN_LEFT, KS0108Module::SCREEN_CENTER, KS0108Module::SCREEN_RIGHT};
const unsigned long DELAY_US = 1;
const byte WIDTH = 192;
const byte HEIGHT = 64;
const byte LENGTH = 1;
byte data[WIDTH];

void setup() {
	pinMode(PROBE, INPUT);
	ks0108Module.initial();
	ks0108Module.clearAll();
	for (byte i = 0; i < sizeof(SCREENS) / sizeof(byte); i++) {
		ks0108Module.selectScreen(SCREENS[i]);
		ks0108Module.setScreen(true);
		ks0108Module.setStartLine(0);
		ks0108Module.setRow(0);
		ks0108Module.setColumn(0);
	}
}

void loop() {
	for (byte j = 0; j < 2; j++) {
		for (byte i = 0; i < WIDTH / LENGTH; i++) {
			byte column = i * LENGTH;
			byte temp = ((j == 0) ? map(analogRead(PROBE), 0, 1023, 0, 63) : data[i]);
			byte tempRow = 7 - temp / 8;
			byte tempData = ((j == 0) ? 1 << (7 - temp % 8) : 0x00);
			ks0108Module.selectScreen(SCREENS[column / 64]);
			ks0108Module.setRow(tempRow);
			ks0108Module.setColumn(column % HEIGHT);
			for (byte k = 0; k < LENGTH; k++) {
				ks0108Module.setData(tempData);
			}
			data[i] = temp;
			delayMicroseconds(DELAY_US);
		}
	}
}