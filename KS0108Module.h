#ifndef KS0108MODULE_H
#define KS0108MODULE_H

#include <Arduino.h>
#include <Font.h>

class KS0108Module {
	private:
		static const byte D_LENGTH = 8;
		static const byte C_LENGTH = 2;
		static const unsigned int DELAY_MICROSECONDS = 50;
		byte rsPin;
		byte rwPin;
		byte enPin;
		byte dPins[KS0108Module::D_LENGTH];
		byte cPins[KS0108Module::C_LENGTH];
		byte rePin;
		void setPin(byte, bool);
		bool getPin(byte);
		void writeByte(byte);
		byte readByte();
		void writeRegister(bool, byte);
		byte readRegister(bool);
	public:
		static const byte SCREEN_NONE = 3;
		static const byte SCREEN_LEFT = 0;
		static const byte SCREEN_CENTER = 1;
		static const byte SCREEN_RIGHT = 2;
		KS0108Module(byte, byte, byte, byte, byte, byte, byte, byte, byte, byte, byte, byte, byte, byte);
		void initial();
		void selectScreen(byte);
		void clear();
		void clearAll();
		void setScreen(bool);
		void setStartLine(byte);
		void setRow(byte);
		void setColumn(byte);
		void setData(byte);
		bool isBusy();
		bool isScreen();
		bool isReset();
		byte getData();
		void drawText(byte, byte, String, Font);
};

#endif