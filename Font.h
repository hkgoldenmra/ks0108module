#ifndef UNIFONTBASIC_H
#define UNIFONTBASIC_H

#include <Arduino.h>

class Font {
	private:
		String name;
		byte rows;
		byte columns;
		Font(String, byte, byte);
	public:
		static const byte MAX_ROWS = 4;
		static const byte MAX_COLUMNS = 32;
		static Font UNIFONT_ASCII();
		static Font UNIFONT_CHINESE();
		String getName();
		byte getRows();
		byte getColumns();
		void getFontMatrix(byte[Font::MAX_ROWS][Font::MAX_COLUMNS], String);
};

#endif