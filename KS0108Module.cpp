#include <KS0108Module.h>

void KS0108Module::setPin(byte pin, bool data) {
	pinMode(pin, OUTPUT);
	digitalWrite(pin, data);
	delayMicroseconds(KS0108Module::DELAY_MICROSECONDS);
}

bool KS0108Module::getPin(byte pin) {
	pinMode(pin, INPUT);
	bool data = digitalRead(pin);
	delayMicroseconds(KS0108Module::DELAY_MICROSECONDS);
	return data;
}

void KS0108Module::writeByte(byte data) {
	this->setPin(this->enPin, HIGH);
	for (byte i = 0; i < KS0108Module::D_LENGTH; i++) {
		this->setPin(this->dPins[i], (data & (1 << i)) > 0);
	}
	this->setPin(this->enPin, LOW);
}

byte KS0108Module::readByte() {
	byte data = 0;
	this->setPin(this->enPin, HIGH);
	for (byte i = 0; i < KS0108Module::D_LENGTH; i++) {
		if (this->getPin(this->dPins[i])) {
			data |= (1 << i);
		}
	}
	this->setPin(this->enPin, LOW);
	return data;
}

void KS0108Module::writeRegister(bool rs, byte data) {
	this->setPin(this->rsPin, rs);
	this->setPin(this->rwPin, LOW);
	this->writeByte(data);
}

byte KS0108Module::readRegister(bool rs) {
	this->setPin(this->rsPin, rs);
	this->setPin(this->rwPin, HIGH);
	return this->readByte();
}

KS0108Module::KS0108Module(byte rsPin, byte rwPin, byte enPin, byte d0Pin, byte d1Pin, byte d2Pin, byte d3Pin, byte d4Pin, byte d5Pin, byte d6Pin, byte d7Pin, byte c0Pin, byte c1Pin, byte rePin) {
	this->rsPin = rsPin;
	this->rwPin = rwPin;
	this->enPin = enPin;
	this->dPins[0] = d0Pin;
	this->dPins[1] = d1Pin;
	this->dPins[2] = d2Pin;
	this->dPins[3] = d3Pin;
	this->dPins[4] = d4Pin;
	this->dPins[5] = d5Pin;
	this->dPins[6] = d6Pin;
	this->dPins[7] = d7Pin;
	this->cPins[0] = c0Pin;
	this->cPins[1] = c1Pin;
	this->rePin = rePin;
}

void KS0108Module::initial() {
	this->setPin(this->rePin, HIGH);
}

void KS0108Module::selectScreen(byte data) {
	if (data == 0) {
		this->setPin(this->cPins[0], LOW);
		this->setPin(this->cPins[1], LOW);
	} else if (data == 1) {
		this->setPin(this->cPins[0], HIGH);
		this->setPin(this->cPins[1], LOW);
	} else if (data == 2) {
		this->setPin(this->cPins[0], LOW);
		this->setPin(this->cPins[1], HIGH);
	} else {
		this->setPin(this->cPins[0], HIGH);
		this->setPin(this->cPins[1], HIGH);
	}
}

void KS0108Module::clear() {
	for (byte r = 0; r < 8; r++) {
		this->setRow(r);
		this->setColumn(0);
		for (byte c = 0; c < 64; c++) {
			this->setData(0x00);
		}
	}
}

void KS0108Module::clearAll() {
	byte sessions[] = {KS0108Module::SCREEN_LEFT, KS0108Module::SCREEN_CENTER, KS0108Module::SCREEN_RIGHT};
	const byte LENGTH = sizeof (sessions) / sizeof (byte);
	for (byte i = 0; i < LENGTH; i++) {
		this->selectScreen(i);
		this->clear();
	}
}

void KS0108Module::setScreen(bool data) {
	this->writeRegister(LOW, 0x3E | data);
}

void KS0108Module::setStartLine(byte data) {
	this->writeRegister(LOW, 0xC0 | (data & 0x3F));
}

void KS0108Module::setRow(byte data) {
	this->writeRegister(LOW, 0xB8 | (data & 0x07));
}

void KS0108Module::setColumn(byte data) {
	this->writeRegister(LOW, 0x40 | (data & 0x3F));
}

void KS0108Module::setData(byte data) {
	this->writeRegister(HIGH, data);
}

bool KS0108Module::isBusy() {
	return (this->readRegister(LOW) & 0x80) > 0;
}

bool KS0108Module::isScreen() {
	return (this->readRegister(LOW) & 0x20) > 0;
}

bool KS0108Module::isReset() {
	return (this->readRegister(LOW) & 0x10) > 0;
}

byte KS0108Module::getData() {
	return this->readRegister(HIGH);
}

void KS0108Module::drawText(byte row, byte column, String text, Font font) {
	byte fontMatrix[Font::MAX_ROWS][Font::MAX_COLUMNS];
	const unsigned int LENGTH = text.length();
	for (unsigned int i = 0, p = 0, l = 0; p < LENGTH; i++, p += l) {
		byte header = text.charAt(0);
		if (header < 0x80) {
			l = 1;
		} else {
			if (header > 0xFC) {
				l = 6;
			} else if (header > 0xF8) {
				l = 5;
			} else if (header > 0xF0) {
				l = 4;
			} else if (header > 0xE0) {
				l = 3;
			} else {
				l = 2;
			}
		}
		font.getFontMatrix(fontMatrix, text.substring(p, p + l));
		for (byte r = 0; r < font.getRows(); r++) {
			this->setRow(r + row);
			this->setColumn((column + i) * font.getColumns());
			for (byte c = 0; c < font.getColumns(); c++) {
				this->setData(fontMatrix[r][c]);
			}
		}
	}
}